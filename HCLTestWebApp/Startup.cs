﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HCLTestWebApp.Startup))]
namespace HCLTestWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
