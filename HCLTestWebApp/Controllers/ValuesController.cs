﻿        using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using HCLTestWebApp.Models;



namespace HCLTestWebApp.Controllers
{
 
    public class ValuesController : ApiController
    {
        private TitlesEntities db = new TitlesEntities();
        /// <summary>
        /// Gets the list of movie titles
        /// </summary>
        /// <param name="name"></param>
        /// <returns>List</returns>
        [Route("MyTitles/ByName/")]
        [ResponseType(typeof(List<Title>))]
        public List<Title> Get(string name)
        {
            var result = db.Title.Where(x => x.TitleName.Contains(name)).ToList();
            return result;
        }

        /// <summary>
        /// Gets details of movie based on the movie title selected
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Movie</returns>
        [Route("MyTitles/Details/")]
        [ResponseType(typeof(Movie))]
        public Movie Get(int id)
        {
            var result = db.Title
                .Include("Award")
                .Include("TitleParticipant")
                .Include("TitleParticipant.Participant")
                .Include("StoryLine")
                .Include("TitleGenre")
                .Where(x => x.TitleId == id)
                .Select(x => new Movie() { AwardWon = x.Award.FirstOrDefault().Award1, AwardYear = x.Award.FirstOrDefault().AwardYear, Description = x.StoryLine.FirstOrDefault().Description, Language = x.StoryLine.FirstOrDefault().Language, ParticipantName = x.TitleParticipant.FirstOrDefault().Participant.Name, ReleaseYear = x.ReleaseYear, TitleId = x.TitleId, TitleName = x.TitleName })
                .FirstOrDefault();
            return result;
        }
    }
}
   