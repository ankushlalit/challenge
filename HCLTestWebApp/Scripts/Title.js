﻿
$(function () {

    function SearchViewModel() {
        var self = this;
        self.searchText = ko.observable();
        self.AllResults = ko.observableArray([]);
        self.MovieDetails = ko.observable();
        self.ErrorText = ko.observable(false);
        self.MovieListLoaded = ko.observable(false);
        self.MovieDetailsLoaded = ko.observable(false);
        self.searchTitlesByName = function () {

            self.MovieDetailsLoaded(false);
            var stext = self.searchText();
            if (stext == undefined || stext == '') {
                self.ErrorText(true);
                self.MovieListLoaded(false);
            }
            else {
                self.ErrorText(false);
                self.MovieListLoaded(true);
                $.getJSON("/MyTitles/ByName/" + "?name=" + stext, function (allData) { self.AllResults(allData); });
            }
        };

        self.GetDetails = function () {
            var selectedRecord = this;
            $.getJSON("/MyTitles/Details/" + "?id=" + selectedRecord.TitleId, function (allData) {
                self.MovieDetails(allData);
                if (self.MovieDetails().TitleId > 0) {
                    self.MovieDetailsLoaded(true);
                }
            });
        };
    }
    ko.applyBindings(new SearchViewModel());
});