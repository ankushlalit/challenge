﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HCLTestWebApp.Models
{
    public class Movie
    {
        public int TitleId { get; set; }

        public string TitleName { get; set; }

        public int? ReleaseYear { get; set; }

        public string Language { get; set; }

        public string Description { get; set; }

        public string ParticipantName { get; set; }

        public string AwardWon { get; set; }

        public int? AwardYear { get; set; }
    }
}